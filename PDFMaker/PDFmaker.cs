﻿using System;
using System.Net;
using System.Text;
using System.Xml;

namespace PDFMaker
{
    public class PDFmaker
    {
        public enum ConvertType
        {
            PDF,
            PNG
        }

        public string ContentType(ConvertType type)
        {
            switch (type) {
                case ConvertType.PDF:
                    return "application/pdf";
                case ConvertType.PNG:
                    return "image/png";
                default:
                    return "text/html";
            }
        }

        public byte[] Convert(string texData, ConvertType convertType, out string error)
        {
            error = null;
            try {
                using (var client = new WebClient()) {
                    /*var cookieContainer = new CookieContainer();
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://sciencesoft.at/latex");
                    request.CookieContainer = cookieContainer;

                    HttpWebResponse response = (HttpWebResponse) request.GetResponse();

                    client.DownloadString("http://sciencesoft.at/latex");

                    if (filesNames != null) {
                        foreach (var fileName in filesNames) {

                            client.Headers["Accept-Encoding"] = "gzip,deflate,sdch";
                            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
                            client.Headers["Content-Disposition"] = "form-data; name=\"myfile\"; filename=\"" + Path.GetFileName(fileName) + "\"";
                            client.Headers["Content-Type"] = "application/postscript";
                            var fileResponceData = client.UploadFile("http://sciencesoft.at/latexUpload/upload", fileName);

                            //var fileResponceData = UploadFileEx(fileName, "http://sciencesoft.at/latexUpload/upload", "myfile", "application/postscript", null, cookieContainer);

                            UploadFilesToRemoteUrl("http://sciencesoft.at/latexUpload/upload", fileName, "");

                            var dataB = Upload("http://sciencesoft.at/latexUpload/upload", File.Open(fileName, FileMode.OpenOrCreate));
                            var fileResponceData = new BinaryReader(dataB).ReadBytes((int)dataB.Length);
                            var fileResponce = Encoding.UTF8.GetString(fileResponceData);
                        }
                    }

                    var data = new NameValueCollection();
                    data["ochem"] = "false";
                    data["runexample"] = "false";
                    data["dpi"] = "120";
                    data["utf8"] = "on";
                    data["template"] = "5";
                    data["device"] = "0";
                    data["bgcolor"] = "";
                    data["papersize"] = "1";
                    data["_"] = "";
                    data["src"] = texData;

                    var responceData = client.UploadValues("http://sciencesoft.at/latex/compile", data);
                    var responce = Encoding.UTF8.GetString(responceData);

                    return client.DownloadData(string.Format("http://sciencesoft.at/image/latex/latex.png?index=89&id=1878899647"));*/

                    client.Encoding = Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "text/xml;charset=utf-8";
                    ServicePointManager.ServerCertificateValidationCallback +=
                        (sender, certificate, chain, sslPolicyErrors) => true;

                    var typeStr = "";
                    switch (convertType) {
                        case ConvertType.PDF:
                            typeStr = "pdfwrite";
                            break;
                        case ConvertType.PNG:
                            typeStr = "png16m";
                            break;
                    }

                    var str = string.Format(
                              "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                              "<latex ochem=\"false\">" +
                              "<dev dpi=\"120\" papersize=\"a4\" bgcolor=\"0\">{0}</dev>" +
                              "<src><![CDATA[{1}]]></src>" +
                              "<embeddedData>true</embeddedData>" +
                              "</latex>", typeStr, texData);

                    var data = Encoding.UTF8.GetBytes(str);
                    var responceData = client.UploadData("http://sciencesoft.at/latex", "PUT", data);

                    var responce = Encoding.UTF8.GetString(responceData);

                    var xmlResponce = new XmlDocument();
                    xmlResponce.LoadXml(responce);

                    var errorMessage = xmlResponce.DocumentElement.SelectSingleNode("error").InnerText;

                    if (!string.IsNullOrEmpty(errorMessage)) {
                        throw new Exception(errorMessage);
                    }

                    var base64encoded = xmlResponce.DocumentElement.SelectSingleNode("data").InnerText;

                    return System.Convert.FromBase64String(base64encoded);
                }
            } catch (Exception e) {
                error = e.Message;
                return null;
            }
        }
    }
}

