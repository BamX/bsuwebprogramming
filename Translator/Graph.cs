﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Translator
{
    class Graph
    {
        public int NodesCount { get; set; }
        public int EdgesCount { get; set; }
        public int LevelsCount { get; set; }
        public int EqCount { get; set; }

        public bool[,,] Edges { get; set; }
        public bool[,,] EqEdges { get; set; }

        public int[,] ZValues { get; set; }
        public int[,] AValues { get; set; }
        public int[] Alpha { get; set; }
        public int[,,,] Lambda { get; set; }

        public IEnumerable<KeyValuePair<int, int>> GetEdges(int level = 0)
        {
            int edgesCount = 0;
            for (int i = 1; i <= NodesCount; ++i) {
                for (int j = 1; j <= NodesCount; ++j) {
                    bool exists = false;
                    for (int k = 1; k <= LevelsCount && !exists; ++k) {
                        exists |= (level == 0 || k == level) && Edges[k, i, j];
                    }
                    if (exists) {
                        yield return new KeyValuePair<int, int>(i, j);
                        ++edgesCount;
                        if (edgesCount == EdgesCount) {
                            yield break;
                        }
                    }
                }
            }
        }

        public Graph(Dictionary<string, object> data)
        {
            NodesCount = (int)data ["|I|"];
            EdgesCount = (int)data ["|U|"];
            LevelsCount = (int)data ["|K|"];
            EqCount = (int)data ["q"];

            Edges = new bool[LevelsCount + 1, NodesCount + 1, NodesCount + 1];
            EqEdges = new bool[LevelsCount + 1, NodesCount + 1, NodesCount + 1];
            ZValues = new int[NodesCount + 1, NodesCount + 1];
            AValues = new int[NodesCount + 1, NodesCount + 1];
            Alpha = new int[EqCount + 1];
            Lambda = new int[EqCount + 1, LevelsCount + 1, NodesCount + 1, NodesCount + 1];

            var pairParser = new Regex (@"(\d+)[,^](\d+)");

            for (int i = 1; i <= LevelsCount; ++i) {
                var dict = data ["widetilde{U}^" + i] as Dictionary<string, object>;
                foreach (var pair in dict) {
                    var match = pairParser.Match (pair.Key);
                    Edges [i, int.Parse (match.Groups [1].Value), int.Parse (match.Groups [2].Value)] = (int)pair.Value == 1;
                }
            }

            for (int i = 1; i <= LevelsCount; ++i) {
                var dict = (data ["widetilde{U}"] as Dictionary<string, object>) ["0^" + i] as Dictionary<string, object>;
                foreach (var pair in dict) {
                    var match = pairParser.Match (pair.Key);
                    EqEdges [i, int.Parse (match.Groups [1].Value), int.Parse (match.Groups [2].Value)] = (int)pair.Value == 1;
                }
            }

            var zDict = data ["z"] as Dictionary<string, object>;
            foreach (var pair in zDict) {
                var match = pairParser.Match (pair.Key);
                ZValues [int.Parse (match.Groups [1].Value), int.Parse (match.Groups [2].Value)] = (int)pair.Value;
            }

            var aDict = data ["a"] as Dictionary<string, object>;
            foreach (var pair in aDict) {
                var match = pairParser.Match (pair.Key);
                AValues [int.Parse (match.Groups [1].Value), int.Parse (match.Groups [2].Value)] = (int)pair.Value;
            }

            var alphaDict = data ["alpha"] as Dictionary<string, object>;
            foreach (var pair in alphaDict) {
                Alpha [int.Parse (pair.Key)] = (int)pair.Value;
            }

            var lambdaDict = data ["lambda"] as Dictionary<string, object>;
            foreach (var pair in lambdaDict) {
                var match = pairParser.Match (pair.Key);
                int x = int.Parse (match.Groups [1].Value);
                int y = int.Parse (match.Groups [2].Value);

                int a = x / 10, b = x % 10;
                int l = y / 10, q = y % 10;

                Lambda [q, l, a, b] = (int)pair.Value;
            }
        }
    }


}

