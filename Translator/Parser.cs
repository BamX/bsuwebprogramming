﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Translator
{
    internal class Parser
    {
        public static Graph ParseGraph(string input)
        {
            var decoded = Parse (input);
            return new Graph (decoded);
        }

        public static void Print(Dictionary<string, object> arr, string tab = "")
        {
            if (arr == null)
                return;

            foreach (var pair in arr) {
                if (pair.Value is Dictionary<string, object>) {
                    Console.WriteLine ("{0}{1}:", tab, pair.Key);
                    Print (pair.Value as Dictionary<string, object>, "\t" + tab);
                } else {
                    Console.WriteLine ("{0}{1}: {2}", tab, pair.Key, pair.Value);
                }
            }
        }

        private static Dictionary<string, object> Parse(string data)
        {
            return ParseGroup (ParsePlain (data), '_').ToDictionary(v => v.Key, v => v.Value);
        }

        private static IEnumerable<KeyValuePair<string, object>> ParseGroup(IEnumerable<KeyValuePair<string, object>> data, char separator)
        {
            return data
                .Select(v => new { splitedKey = v.Key.Split(separator), value = v})
                .GroupBy (v => v.splitedKey.First())
                .Select (g => {
                    var ar = g.ToArray ();
                    if (ar.First().splitedKey.Length > 1) {
                        return new KeyValuePair<string, object> (g.Key, g.ToDictionary (v => v.value.Key.Substring (g.Key.Length + 1), v => v.value.Value));
                    } else {
                        return ar.First ().value;
                    }
                });
        }

        private static IEnumerable<KeyValuePair<string, object>> ParsePlain(string data)
        {
            var lines = data.Split ('\n').Where(s => s.Length > 0).ToArray();
            string parseArrayName = null;
            Dictionary<string, object> parseArrayValues = null;
            var simpleRegex = new Regex (@"/\*([^\*]*)\*/([^\n\r]*)");
            var arrayEmRegex = new Regex (@"/\*(\{\d+,\d+\})\*/([^\n\r]*)");

            foreach (var line in lines) {
                if (parseArrayName != null) {
                    var matchEm = arrayEmRegex.Match (line);
                    if (matchEm.Success) {
                        var keyEm = matchEm.Groups [1].Value;
                        var valueStringEm = matchEm.Groups [2].Value;

                        parseArrayValues.Add (keyEm, int.Parse(valueStringEm));
                        continue;
                    } else {
                        if (parseArrayValues.Count > 0) {
                            yield return new KeyValuePair<string, object> (parseArrayName, parseArrayValues);
                        }
                        parseArrayName = null;
                    }
                }

                var match = simpleRegex.Match (line);
                var key = match.Groups [1].Value;
                var valueString = match.Groups [2].Value;

                if (valueString.Length > 0) {
                    yield return new KeyValuePair<string, object> (key, int.Parse (valueString));
                } else {
                    parseArrayName = key;
                    parseArrayValues = new Dictionary<string, object> ();
                }
            }
        }
    }
}

