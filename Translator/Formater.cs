﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Translator
{
    internal class Formater
    {
        public static string Format(Graph graph)
        {
            var result = "";
            result += Header();

            result += SmallEq(graph);

            result += Graph(graph);

            result += BigEq(graph);
            result += SimpleEq(graph);



            result += Footer();
            return result;
        }

        private static string Header()
        {
            return 
                "\\documentclass{article}\n" +
                "\\usepackage{amssymb,amsmath}\n" +
                "\\usepackage[english,russian]{babel}\n" +
                "\\usepackage{graphicx}\n" +
                "\\usepackage{tikz}\n" +
                "\\usetikzlibrary{arrows}\n" +
                "\\begin{document}\n" +
                "\\begin{center}\n";
        }

        private static string Footer()
        {
            return 
                "\\end{center}\n" +
                "\\end{document}";
        }

        private static string Graph(Graph graph)
        {
            var result = "";
            var edgeFormats = new [] { 
                "{black,draw=black,very thick}",
                "{blue,draw=blue,very thick}",
                "{red,draw=red,very thick}",
            };
                
            var cFormats = new [] {
                new string[0],
                new [] { "edge[->]" },
                new [] { "edge[->,bend left]", "edge[->,bend right]" },
                new [] { "edge[->]", "edge[->,bend left]", "edge[->,bend right]" },
            };

            int graphsNum = (int)Math.Ceiling(graph.LevelsCount / (float)edgeFormats.Length);
            for (int k = 0; k < graphsNum; ++k) {
                result +=
                    "\\begin{tikzpicture}\n" +
                    "\\tikzset{vertex/.style = {shape=circle,draw,minimum size=7pt}}\n" +
                    "\\tikzset{edge/.style = {->,> = latex'}}\n";

                result += string.Format("\\def \\z {0}\n", "{" + graph.NodesCount + "}");

                result += 
                    "\\foreach \\s in {1,...,\\z} {\n" +
                    "    \\node[draw, circle, minimum size=1cm] (N-\\s) at ({360/\\z * (\\s - 1)}:3cm) {$\\s$};\n" +
                    "}\n";

                int[,] counts = new int[graph.NodesCount + 1, graph.NodesCount + 1];
                for (int i = 0; i < edgeFormats.Length; ++i) {
                    foreach (var edge in graph.GetEdges(i + 1 + k * edgeFormats.Length)) {
                        ++counts[edge.Key, edge.Value];
                        ++counts[edge.Value, edge.Key];
                    }
                }
                int[,] reCount = new int[graph.NodesCount + 1, graph.NodesCount + 1];

                for (int i = 0; i < edgeFormats.Length; ++i) {
                    foreach (var edge in graph.GetEdges(i + 1 + k * edgeFormats.Length)) {
                        result += 
                        string.Format(
                                "\\path[every edge/.style={2}] (N-{0}) {3} (N-{1});\n", 
                                edge.Key, 
                                edge.Value, 
                                edgeFormats[i], 
                                cFormats[counts[edge.Key, edge.Value]][reCount[edge.Key, edge.Value]]);
                        reCount[edge.Key, edge.Value]++;
                        reCount[edge.Value, edge.Key]++;
                    }
                }

                result += "\\end{tikzpicture}\n";
            }
            return result;
        }

        private static string SmallEq(Graph graph)
        {
            var result = "";

            for (int k = 1; k <= graph.LevelsCount; ++k) {
                for (int i = 1; i <= graph.NodesCount; ++i) {
                    result += "$";
                    bool one = false;
                    for (int j = 1; j <= graph.NodesCount; ++j) {
                        if (graph.Edges[k, i, j]) {
                            result += (one ? "+" : "") + "x_{" + i + "," + j + "}^" + k;
                            one = true;
                        } else if (graph.Edges[k, j, i]) {
                            result += (one ? "-" : "") + "x_{" + j + "," + i + "}^" + k;
                            one = true;
                        }
                    }
                    if (one) {
                        result += "=" + graph.AValues[i, k];
                    }
                    result += " $";
                    if (one) {
                        result += "\\\\ \n";
                    } else {
                        result = result.Substring(0, result.Length - 3);
                    }
                }
                result += "\\bigskip";
            }

            return result;
        }

        private static string BigEq(Graph graph)
        {
            var result = "";

            for (int k = 1; k <= graph.EqCount; ++k) {
                result += "$";
                bool one = false;
                for (int i = 1; i <= graph.NodesCount; ++i) {
                    for (int j = 1; j <= graph.NodesCount; ++j) {
                        for (int l = 1; l <= graph.LevelsCount; ++l) {
                            var coef = graph.Lambda[k, l, i, j];
                            if (coef != 0) {
                                result += 
                                    (one ? (coef > 0 ? "+" : "") : "") + 
                                    (coef != 1 ? coef.ToString() : "") + 
                                    "x_{" + i + "," + j + "}^{" + l + "}";
                                one = true;
                            }
                        }
                    }
                }
                result += "=" + graph.Alpha[k] + "$\\\\ \\bigskip\n";
            }
            result += "\\bigskip";

            return result;
        }

        private static string SimpleEq(Graph graph)
        {
            var result = "";

            for (int i = 1; i <= graph.NodesCount; ++i) {
                for (int j = 1; j <= graph.NodesCount; ++j) {
                    var coef = graph.ZValues[i, j];
                    bool any = false;
                    for (int k = 1; k <= graph.LevelsCount; ++k) {
                        if (graph.EqEdges[k, i, j]) {
                            if (!any) {
                                result += "$";
                            }
                            result += (any ? "+" : "") + "x_{"+i+","+j+"}^{"+k+"}";
                            any = true;
                        }
                    }
                    if (any) {
                        result += "=" + coef + "$ \\\\";
                    }
                }
            }

            result += "\\bigskip";
            return result;
        }

        private static string Table(Graph graph)
        {
            var result = "\\begin{tabular}";

            result += "{|l|" + Enumerable.Range(0, graph.EdgesCount).Select(i => "|c|c|").Sum() + "}\n";
            result += "\\hline$(i, j)$ " + Enumerable.Range(0, graph.EdgesCount).Select(i => " &   \\multicolumn{2}{|c||}{$(0, 1)$}").Sum() + "\\\\\\hline";

            result += 
                "\\end{tabular}\n" +
                "\\\\\\bigskip";
            return result;
        }
    }

    static class Extensions
    {
        public static string Sum(this IEnumerable<string> self) 
        {
            return self.Aggregate((a,b) => a + b);
        }
    }
}

