﻿using System;

namespace Translator
{
    public class Translator
    {
        public string Translate(string input, out string error)
        {
            error = null;
            try {
                var graph = Parser.ParseGraph(input);
                return Formater.Format(graph);
            } catch (Exception e) {
                error = e.Message;
                return null;
            }
        }
    }
}

