﻿using System;
using System.IO;

namespace ConsoleConverter
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            if (args.Length != 1) {
                Console.WriteLine("Usage: ConsoleConverter.exe [filename]\n\t[filename] - file with input.txt content");
                return;
            }

            var filename = args[0];
            var translator = new Translator.Translator();
            var converter = new PDFMaker.PDFmaker();

            if (!File.Exists(filename)) {
                Console.Error.WriteLine("File '{0}' not found", filename);
                return;
            }

            using (var sr = new StreamReader(filename)) {
                var inputString = sr.ReadToEnd();
                string error;

                Console.Write("Transalting... ");
                var texString = translator.Translate(inputString, out error);

                if (error != null) {
                    Console.Error.WriteLine("Can't translate:\n" + error);
                    return;
                }
                Console.WriteLine("OK");

                Console.Write("Converting... ");
                var data = converter.Convert(texString, PDFMaker.PDFmaker.ConvertType.PDF, out error);

                if (error != null) {
                    Console.Error.WriteLine("Can't build pdf:\n" + error);
                    return;
                } 
                Console.WriteLine("OK");

                Console.Write("Writing output.pdf...");
                try {
                    using (var bw = new BinaryWriter(File.OpenWrite("output.pdf"))) {
                        bw.Write(data);
                    }
                    Console.WriteLine("OK");
                } catch (Exception e) {
                    Console.Error.WriteLine("Can't frite file:\n" + e.Message);
                    return;
                }
            }
        }
    }
}
