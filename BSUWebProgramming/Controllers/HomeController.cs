﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Schema;
using System.Threading;

namespace BSUWebProgramming
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("Converter");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Converter()
        {
            ViewBag.Data = "";
            ViewBag.Error = null;
            ViewBag.ConvertType = PDFMaker.PDFmaker.ConvertType.PDF;

            return View();
        }

        [HttpPost]
        public ActionResult Converter(string inputString, PDFMaker.PDFmaker.ConvertType convertType)
        {
            var translator = new Translator.Translator();
            var converter = new PDFMaker.PDFmaker();

            string error;

            var texString = translator.Translate(inputString, out error);
            if (error == null) {
                var data = converter.Convert(texString, convertType, out error);
                if (error == null) {
                    return new FileContentResult(data, converter.ContentType(convertType));
                }
            }

            ViewBag.Data = inputString;
            ViewBag.ConvertType = convertType;
            ViewBag.Error = error;
            return View();
        }

        [HttpPost]
        public ActionResult Latex(string inputString)
        {
            ViewBag.Data = inputString;
            ViewBag.ConvertType = PDFMaker.PDFmaker.ConvertType.PDF;

            var translator = new Translator.Translator();

            string error;

            var texString = translator.Translate(inputString, out error);
            if (error == null) {
                ViewBag.Tex = texString;
                return View();
            } else {
                ViewBag.Error = error;
                return View("Converter");
            }
        }

        private const string mathFilename_ = "mathfile.nb";
        
        private const string executeTemplate = "execute_template.m";
        private const string executeFilename = "execute.m";
        private const string mathfilePathVar_ = "MathfilePath";

        private const string batfileTemplate_ = "run_template.bat";
        private const string batfilename_ = "run.bat";
        private const string executePathVar_ = "ExecutefilePath";

        private string appdata_;
        private string workingDir_;
        
        // GET: /Home/
        public ActionResult Mathematica()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Mathematica(HttpPostedFileBase file)
        {
            appdata_ = Server.MapPath("~/AppData").Replace('\\', '/');
            workingDir_ = Path.Combine(Server.MapPath("~/AppData"), Guid.NewGuid().ToString()).Replace('\\', '/');
            Directory.CreateDirectory(workingDir_);
            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.Combine(workingDir_, mathFilename_);
                file.SaveAs(filename);
                GenerateExecuteFile();
                GenerateBatchFile();
                var files = RunBatFile();
                files = files ?? new string[0];
                var solution = files.FirstOrDefault(_ => _.Contains("solution.png")) ?? @"C:\Program Files (x86)\Jenkins\jobs\BSUWebProgramming\workspace\BSUWebProgramming\AppData\solution.png";
                var simplify = files.FirstOrDefault(_ => _.Contains("simplify.png")) ?? @"C:\Program Files (x86)\Jenkins\jobs\BSUWebProgramming\workspace\BSUWebProgramming\AppData\simplify.png";
                return RedirectToAction("MatAnswer", new { solution, simplify });
            }
            
            // redirect back to the index action to show the form once again
            return RedirectToAction("MatAnswer");
        }

        public ActionResult MatAnswer(string solution, string simplify)
        {
            ViewBag.Solution = 
                solution != null ? 
                Convert.ToBase64String(System.IO.File.ReadAllBytes(solution)) : "";
            ViewBag.Simplify = 
                simplify != null ? 
                Convert.ToBase64String(System.IO.File.ReadAllBytes(simplify)) : "";
            return View();
        }

        private void GenerateExecuteFile()
        {
            var settings = new GenerationSettings
            {
                Appdata = appdata_,
                WorkingDir = workingDir_,
                TemplateName = executeTemplate,
                FileName = executeFilename,
                VarName = mathfilePathVar_,
                VarValue = mathFilename_
            };
            GenerateFile(settings);
        }

        private void GenerateBatchFile()
        {
            var settings = new GenerationSettings
            {
                Appdata = appdata_,
                WorkingDir = workingDir_,
                TemplateName = batfileTemplate_,
                FileName = batfilename_,
                VarName = executePathVar_,
                VarValue = executeFilename
            };
            GenerateFile(settings);
        }

        private void GenerateFile(GenerationSettings settings)
        {
            var content = "";
            using (var reader = new StreamReader(new FileStream(settings.TemplateName, FileMode.Open, FileAccess.Read)))
            {
                content = reader.ReadToEnd();
            }
            content = content.Replace(settings.VarName, settings.VarValue);
            using (var writer = new StreamWriter(new FileStream(settings.FileName, FileMode.Create, FileAccess.Write)))
            {
                writer.Write(content);
            }
        }

        private string[] RunBatFile()
        {
            ProcessStartInfo processInfo;
            Process process;

            var path = Path.Combine(workingDir_, batfilename_);
            processInfo = new ProcessStartInfo(path);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            process = Process.Start(processInfo);
            process.WaitForExit();

            for (var i = 5; i > 0; --i)
            {
                var files = Directory.GetFiles(workingDir_, "*.png");
                if (files.Length > 0)
                    return files;
                Thread.Sleep(3000);
            }
            return null;
        }
    }

    public class GenerationSettings
    {
        private string value_;
        private string templateName_;
        private string fileName_;

        public string TemplateName
        {
            get { return Path.Combine(Appdata, templateName_).Replace('\\', '/'); }
            set { templateName_ = value; }
        }

        public string Appdata { get; set; }

        public string WorkingDir { get; set; }

        public string VarName { get; set; }

        public string VarValue
        {
            get { return Path.Combine(WorkingDir, value_).Replace('\\', '/'); }
            set { value_ = value; }
        }

        public string FileName
        {
            get { return Path.Combine(WorkingDir, fileName_).Replace('\\', '/'); }
            set { fileName_ = value; }
        }
    }
}

