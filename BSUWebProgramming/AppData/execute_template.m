Needs["JLink`"];
$FrontEndLaunchCommand="Mathematica.exe";
UseFrontEnd[
nb = NotebookOpen["MathfilePath"];
SelectionMove[nb, All, Notebook];
SelectionEvaluate[nb];
];
Pause[10]
CloseFrontEnd[];